import React, {Fragment} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Image,
  ImageBackground
} from 'react-native';

const Login = () => {
  return (
    <ImageBackground source={require('../assets/img/login-bg.png')} style={styles.container}>
        <Image source={require('../assets/img/logo.png')} style={styles.logo} />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex : 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 300,
    height: 60
  }
});

export default Login;
